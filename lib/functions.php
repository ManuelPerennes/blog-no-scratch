<?php
function getArticles(){
    require('../lib/connexion.php');
    $req = $bdd->prepare('SELECT * FROM articles ORDER BY id DESC');
    $req->execute();
    $data = $req->fetchAll(PDO::FETCH_OBJ);
    return $data;
    $req->closeCursor();
}

function leHeader (){
	include __DIR__ . '/../include/header.php';
}

function leFooter(){
	include __DIR__ . '/../include/footer.php';
}

